import Point from "./utils/Point";
import Triangle from "./utils/Triangle";
import Plane from "./graph/plane";
import Window from "./utils/Window";

export const app = () => {
    console.time("App");

    const shapes = [
        new Triangle(
            new Point(17, 17),
            new Point(14, 14),
            new Point(11, 17),
        ),
        new Triangle(
            new Point(11, 11),
            new Point(14, 14),
            new Point(17, 11),
        ),
        new Triangle(
            new Point(21, 21),
            new Point(24, 24),
            new Point(27, 21),
        ),
        new Triangle(
            new Point(27, 27),
            new Point(24, 24),
            new Point(21, 27),
        ),
    ]

    // console.clear();
    // Plane.generatePlaneByShapes.apply(Plane, shapes);
    // console.timeEnd("App");

    new Window(600, 400, "Test window");
}

