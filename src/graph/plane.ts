import Point from "../utils/Point";
import Triangle from "../utils/Triangle";

interface Size {
    highX: number,
    lowX: number,
    highY: number,
    lowY: number,
}

class Plane {
    public visiblePart: Size = {
        highX: 0,
        lowX: 0,
        highY: 0,
        lowY: 0,
    };
    public points: Array<Point>;

    constructor(points: Array<Point>) {
        this.points = points;
        this.setVisiblePart();
        this.viewPlane();
    }

    setVisiblePart() {
        this.visiblePart.highX = process.stdout.columns;
        this.visiblePart.highY = Math.max(process.stdout.rows/2, 25);
    }

    viewPlane() {
        let result = "";

        let pointIter = this.points.length-1;

        let y = this.visiblePart.highY;
        for(y; y > this.visiblePart.lowY; y--) {
            let x = this.visiblePart.lowX;
            for (x; x < this.visiblePart.highX; x++) {
                if (this.points.some(p => new Point(x, y).asArray().every((n, i) => n === p.asArray()[i]))) {
                    result += "\x1b[45m ";
                    pointIter = Math.max(pointIter - 1, 0);
                } else {
                    result += `\x1b[44m `;
                }
            }
        }
        result += "\x1b[0m"
        console.log(result);
    }

    static generatePlaneByShapes(...triangles: Array<Triangle>) {
        let points = [];

        triangles.forEach(triangle => {
            points = points.concat(triangle.points);
        })

        return new this(points);
    }
}

export default Plane;
