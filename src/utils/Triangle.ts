import Point from "./Point";
import Line from "./Line";

interface TriangleVertices {
    a: Point;
    b: Point;
    c: Point;
}

interface TriangleLines {
    ab: Line;
    ac: Line;
    bc: Line;
}

class Triangle {
    public vertices: TriangleVertices;
    public lines: TriangleLines = {
        ab: null,
        ac: null,
        bc: null,
    };
    public p: Set<string> = new Set<string>();
    public perimeterPoints: Array<Point> = [];

    constructor(a: Point, b: Point, c: Point) {
        this.vertices = {a, b, c}
        this.createLines();
    }

    private createLines() {
        this.lines.ab = new Line(this.vertices.a, this.vertices.b);
        this.lines.ac = new Line(this.vertices.a, this.vertices.c);
        this.lines.bc = new Line(this.vertices.b, this.vertices.c);
        this.perimeterPoints = [
            ...this.lines.ab.linePoints,
            ...this.lines.ac.linePoints,
            ...this.lines.bc.linePoints,
        ];
        this.createTriangleArea();
    }

    private createTriangleArea() {
        const xArray = this.perimeterPoints.map((p) => p.X);
        const high = Math.max.apply(this, xArray) + 1;
        const min = Math.min.apply(this, xArray);
        for (let i = min; i < high; i++) {
            this.addPointsByX(i);
        }
    }

    private addPointsByX(x) {
        const lineArea = this.perimeterPoints.filter((p) => p.X === x);
        if (lineArea.length > 1) {
            const min = Math.min(lineArea[0].Y, lineArea[1].Y);
            const max = Math.max(lineArea[0].Y, lineArea[1].Y);

            lineArea.forEach(p => this.addUniquePoint(p.asArray()))

            for (let y = min; y < max; y++) {
                this.addUniquePoint([x, y]);
            }
        } else {
            this.addUniquePoint(lineArea[0].asArray());
        }
    }

    addUniquePoint(point: [number, number]) {
        if (!this.p.has(JSON.stringify(point))) {
            this.p.add(JSON.stringify(point));
        }
    }

    get points(): Array<Point> {
        return [...this.p].map(point => Point.createFromArray(JSON.parse(point) as [number, number]))
    }
}

export default Triangle;
