import Point from "./Point";
import Vector from "./Vector";

class Line {
    public vector: Vector;
    public linePoints: Array<Point> = [];
    private firstPoint: Point;
    private lastPoint: Point;
    private equationAxis: "X" | "Y";

    constructor(pointA: Point, pointB: Point) {
        this.vector = new Vector(pointA, pointB);
        this.createLine();
    }

    createLine() {
        this.firstPoint = this.vector.pointA.isLowerByXThan(this.vector.pointB) || this.vector.pointA.isLowerByYThan(this.vector.pointB)
            ? this.vector.pointA
            : this.vector.pointB;

        this.lastPoint = this.firstPoint.isEqual(this.vector.pointA)
            ? this.vector.pointB
            : this.vector.pointA;

        this.equationAxis = this.firstPoint.isLowerByXThan(this.lastPoint)
            ? "X"
            : "Y";

        this.linePoints.push(this.firstPoint);
        this.addPoint();
    }

    getLastPoint(): Point {
        return this.linePoints[this.linePoints.length - 1];
    }

    addPoint() {
        const point = this.vector.equation[`getPointBy${this.equationAxis}`](this.getLastPoint()[this.equationAxis] + 1);
        if (this.lastPoint[`isLowerBy${this.equationAxis}Than`](point) || this.lastPoint.isEqual(point)) {
            this.linePoints.push(this.lastPoint);
            return;
        }
        this.linePoints.push(point);
        this.addPoint();
    }
}

export default Line;
