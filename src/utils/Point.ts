class Point {
    constructor(public X: number, public Y: number) {}

    static createFromArray(points: [number, number]): Point {
        return new this(points[0], points[1]);
    }

    asArray(): [number, number] {
        return [this.X, this.Y];
    }

    isEqual(point: Point) {
        return this.X === point.X && this.Y === point.Y;
    }

    isLowerThan(point: Point) {
        return this.isLowerByXThan(point) && this.isLowerByYThan(point);
    }

    isLowerByXThan(point: Point) {
        return this.X < point.X;
    }

    isLowerByYThan(point: Point) {
        return this.Y < point.Y;
    }
}

export default Point;
