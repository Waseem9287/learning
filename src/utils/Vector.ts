import Point from "./Point";
import Equation from "./Equation";

class Vector {
    public equation: Equation;

    constructor(public pointA: Point, public pointB: Point) {
        this.createEquation();
    }

    createEquation() {
        this.equation = new Equation(
            this.pointA,
            this.pointB
        );
    }
}


export default Vector;
