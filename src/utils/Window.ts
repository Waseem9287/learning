import SDL2 from "../libs/sdl2";
import Triangle from "./Triangle";
import Point from "./Point";

class Window {
    public quit = false;

    private sdlWindowLink;
    private renderer;

    points = new Triangle(
        new Point(0, 0),
        new Point(100, 100),
        new Point(0, 200),
    ).points;

    constructor(public width: number, public height: number, public name: string) {
        console.log(SDL2);
        this.initializeWindow();
    }

    private initializeWindow() {
        SDL2.SDL_Init(SDL2.SDL_INIT_EVERYTHING);

        this.sdlWindowLink = SDL2.SDL_CreateWindow(this.name,
            0, 0, this.width, this.height, SDL2.SDL_WINDOW_SHOWN | SDL2.SDL_WINDOW_RESIZABLE);

        this.renderer = SDL2.SDL_CreateRenderer(this.sdlWindowLink, -1, SDL2.SDL_RENDERER_ACCELERATED);

        //Initialize renderer color
        SDL2.SDL_SetRenderDrawColor(this.renderer, [0x00, 0x00, 0x00, 0x00]);

        while (!this.quit) {
            let event: {
                event: string,
                type: string,
            } = {
                event: null,
                type: null,
            };
            SDL2.SDL_PumpEvents();
            while (1) {
                let ret = SDL2.SDL_PeepEvents(event, 1, SDL2.SDL_GETEVENT, SDL2.SDL_FIRSTEVENT, SDL2.SDL_LASTEVENT);
                if (ret == 1) break;
                SDL2.SDL_Delay(10);
                this.render();
                SDL2.SDL_PumpEvents();
            }

            switch (event.type) {
                case "MOUSEBUTTONDOWN":
                    break;
                case "MOUSEBUTTONUP":
                    break;
                case "MOUSEWHEEL":
                    break;
                case "WINDOWEVENT":
                    if (event.event == "WINDOWEVENT_RESIZED") {
                        [this.width, this.height] = SDL2.SDL_GetWindowSize(this.sdlWindowLink);

                        this.render();
                    } else if (event.event == "WINDOWEVENT_SIZE_CHANGED") {

                    } else if (event.event == "WINDOWEVENT_EXPOSED") {
                    }
                    break;
                case "KEYDOWN":
                    break;
                case "QUIT":
                    this.quit = true;
                    break;
            }
        }
        SDL2.SDL_DestroyRenderer(this.renderer);
        SDL2.SDL_DestroyWindow(this.sdlWindowLink);
        SDL2.SDL_Quit();
    }

    render() {
        //Clear screen
        SDL2.SDL_SetRenderDrawColor(this.renderer, [0x00, 0x00, 0x00, 0x00]);
        SDL2.SDL_RenderClear(this.renderer);

        //Render red filled quad
        SDL2.SDL_SetRenderDrawColor(this.renderer, [0xFF, 0x00, 0x00, 0xFF]);
        SDL2.SDL_RenderDrawPoints(this.renderer, this.points.map(p => p.asArray()), this.points.length);

        //Update screen
        SDL2.SDL_RenderPresent(this.renderer);
    }
}

export default Window;
