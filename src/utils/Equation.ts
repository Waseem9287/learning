import Point from "./Point";


// AX + BY + C = 0

class Equation {
    public a: number;
    public b: number;
    public c: number;

    constructor(pointA: Point, pointB: Point) {
        this.a = pointA.Y - pointB.Y;
        this.b = pointB.X - pointA.X;
        this.c = (pointA.X * pointB.Y) - (pointB.X * pointA.Y)
    }

    getYByX(x) {
        return ((this.a * x) + this.c) / -this.b;
    }

    getXByY(y) {
        return ((this.b * y) + this.c) / -this.a;
    }

    getPointByX(x) {
        return new Point(+x.toFixed(), +this.getYByX(x).toFixed());
    }

    getPointByY(y) {
        return new Point(+this.getXByY(y).toFixed(), +y.toFixed());
    }
}

export default Equation;
