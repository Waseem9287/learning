// TODO: Works intensive with types

declare module SDL2 {

    interface SDL2 {
        readonly SDL_INIT_VIDEO: 32;
        readonly SDL_INIT_EVERYTHING: 62001;
        readonly SDL_QUIT: 256;
        readonly SDL_WINDOWPOS_UNDEFINED: 536805376;
        readonly SDL_WINDOW_RESIZABLE: 32;
        readonly SDL_WINDOW_OPENGL: 2;
        readonly SDL_WINDOW_SHOWN: 4;
        readonly SDL_PIXELFORMAT_ARGB8888: 372645892;
        readonly SDL_WINDOWEVENT: 512;
        readonly SDL_MOUSEWHEEL: 1027;
        readonly SDL_WINDOWEVENT_RESIZED: 5;
        readonly SDL_MOUSEBUTTONDOWN: 1025;
        readonly SDL_KEYDOWN: 768;
        readonly SDL_TEXTINPUT: 771;
        readonly SDL_TEXTEDITING: 770;
        readonly SDL_FIRSTEVENT: 0;
        readonly SDL_LASTEVENT: 65535;
        readonly SDL_ADDEVENT: 0;
        readonly SDL_PEEKEVENT: 1;
        readonly SDL_GETEVENT: 2;
        readonly SDLK_ESCAPE: 27;
        readonly SDLK_LEFT: 1073741904;
        readonly SDLK_RIGHT: 1073741903;
        readonly SDLK_UP: 1073741906;
        readonly SDLK_DOWN: 1073741905;
        readonly SDLK_RETURN: 13;
        readonly SDLK_TAB: 9;
        readonly SDLK_BACKSPACE: 8;
        readonly SDLK_DELETE: 127;
        readonly KMOD_LSHIFT: 1;
        readonly KMOD_RSHIFT: 2;
        readonly KMOD_LCTRL: 64;
        readonly KMOD_RCTRL: 128;
        readonly KMOD_LALT: 256;
        readonly KMOD_RALT: 512;
        readonly KMOD_CAPS: 8192;
        readonly KMOD_CTRL: 192;
        readonly KMOD_SHIFT: 3;
        readonly KMOD_ALT: 768;
        readonly SDL_GL_CONTEXT_FLAGS: 20;
        readonly SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG: 2;
        readonly SDL_GL_DOUBLEBUFFER: 5;
        readonly SDL_GL_MULTISAMPLEBUFFERS: 13;
        readonly SDL_GL_MULTISAMPLESAMPLES: 14;
        readonly SDL_GL_DEPTH_SIZE: 6;
        readonly SDL_GL_STENCIL_SIZE: 7;
        readonly SDL_GL_CONTEXT_MAJOR_VERSION: 17;
        readonly SDL_GL_CONTEXT_MINOR_VERSION: 18;
        readonly SDL_SYSTEM_CURSOR_ARROW: 0;
        readonly SDL_SYSTEM_CURSOR_IBEAM: 1;
        readonly SDL_SYSTEM_CURSOR_WAIT: 2;
        readonly SDL_SYSTEM_CURSOR_CROSSHAIR: 3;
        readonly SDL_SYSTEM_CURSOR_WAITARROW: 4;
        readonly SDL_SYSTEM_CURSOR_SIZENWSE: 5;
        readonly SDL_SYSTEM_CURSOR_SIZENESW: 6;
        readonly SDL_SYSTEM_CURSOR_SIZEWE: 7;
        readonly SDL_SYSTEM_CURSOR_SIZENS: 8;
        readonly SDL_SYSTEM_CURSOR_SIZEALL: 9;
        readonly SDL_SYSTEM_CURSOR_NO: 10;
        readonly SDL_SYSTEM_CURSOR_HAND: 11;


        SDL_GetWindowSize(window: SDLWindow): SDLWindowSize;
        SDL_GetWindowSize(window: SDLWindow, width: number): SDLWindowSize;
        SDL_GetWindowSize(window: SDLWindow, width: number, height: number): SDLWindowSize;
    }

    // TODO: Add SDL flags
    interface SDLFlags {

    }

    // TODO: Add SDL window
    interface SDLWindow {

    }

    type SDLWindowSize = [number, number]


    function SDL_Init(initialSettings: number): void;
    function SDL_Quit(): void;
    function SDL_CreateWindow(windowName: string, x: number, y: number, width: number, height: number, flags: SDLFlags): SDLWindow;
    function SDL_DestroyWindow(window: SDLWindow): void;
    function SDL_SetWindowSize(width: number, height: number): void;

    function SDL_GetWindowSurface(window: SDLWindow): void;

    function SDL_CreateRenderer(): void;
    function SDL_DestroyRenderer(): void;
    function SDL_RenderClear(): void;
    function SDL_RenderPresent(): void;
    function SDL_SetRenderDrawColor(): void;
    function SDL_RenderCopy(): void;
    function SDL_RenderFillRect(): void;
    function SDL_CreateTexture(): void;
    function SDL_DestroyTexture(): void;
    function SDL_UpdateTexture(): void;
    function SDL_Delay(): void;
    function SDL_GetTicks(): void;
    function SDL_PumpEvents(): void;
    function SDL_WaitEvent(): void;
    function SDL_PollEvent(): void;
    function SDL_PushEvent(): void;
    function SDL_PeepEvents(): void;
    function SDL_RegisterEvents(): void;
    function SDL_StartTextInput(): void;
    function SDL_StopTextInput(): void;
    function SDL_SetTextInputRect(): void;
    function SDL_ShowCursor(): void;
    function SDL_GetCursor(): void;
    function SDL_SetCursor(): void;
    function SDL_CreateCursor(): void;
    function SDL_CreateSystemCursor(): void;
    function SDL_GL_SetAttribute(): void;
    function SDL_GL_CreateContext(): void;
    function SDL_GL_SetSwapInterval(): void;
    function SDL_GL_SwapWindow(): void;
}